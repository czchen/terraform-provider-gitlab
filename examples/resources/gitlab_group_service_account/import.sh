# You can import a group service account using `terraform import <resource> <id>`.  The
# `id` is in the form of <group_id>:<service_account_id>
terraform import gitlab_group_service_account.example example
