---
page_title: "GitLab Provider"
subcategory: ""
description: |-
  Terraform Provider for GitLab
---

Use the GitLab provider to interact with GitLab resources, like
users, groups, projects and more. You must configure the provider with
the proper credentials before you can use it.

The provider uses the [`xanzy/go-gitlab`](https://github.com/xanzy/go-gitlab) library
to interact with the [GitLab REST API](https://docs.gitlab.com/ee/api/api_resources.html).

Each data source and resource references the appropriate upstream GitLab REST API documentation,
which may be consumed to better understand the behavior of the API.

Use the navigation to the left to read about the valid data sources and resources.

This provider requires at least [Terraform 1.0](https://www.terraform.io/downloads.html).
A minimum of Terraform 1.4.0 is recommended.

-> Using a Project or Group access token may cause issues with some resources, as those token types don't 
have full access to every API. This is also true when using a `CI_JOB_TOKEN`. Consider using a dedicated
Personal Access Token or Service Account if you are experiencing permission errors when adding resources. 

## Example Usage

{{tffile "examples/provider/provider.tf"}}

{{ .SchemaMarkdown | trimspace }}
